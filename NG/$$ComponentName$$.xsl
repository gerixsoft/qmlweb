<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="text" indent="no" encoding="UTF-8" />

	<xsl:template match="/">
		<xsl:for-each select="qml/Module/Component">
			<xsl:value-of select="concat(@ComponentName, '&#10;')" />
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>
    		