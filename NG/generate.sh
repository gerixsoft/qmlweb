qtversion="5.3"

echo "Generate XML0"
rm -rf output.xml0
mkdir -p output.xml0
./bin/qmlplugindump -notrelocatable -builtins>output.xml0/Qt.xml
xmllint --noout output.xml0/Qt.xml
for qmlplugin in QtMultimedia; do
	./bin/qmlplugindump -notrelocatable $qmlplugin $qtversion>output.xml0/$qmlplugin.xml
	xmllint --noout output.xml0/$qmlplugin.xml
done

echo "Generate XML1"
rm -rf output.xml1
mkdir -p output.xml1
for xml0_file in output.xml0/*.xml; do
	xml_file=output.xml1/${xml0_file#output.xml0/}
	xsltproc --output $xml_file preprocess0.xsl $xml0_file
done

echo "Generate XML1"
rm -rf output.xml
mkdir -p output.xml
for xml0_file in output.xml1/*.xml; do
	xml_file=output.xml/${xml0_file#output.xml1/}
	xsltproc --output $xml_file preprocess1.xsl $xml0_file
done

echo "Generate XSL"
rm -rf output.xsl
for js_file in $(find template -type f); do
	xsl_file=output.xsl/${js_file#template/}.xsl
	mkdir -p "$(dirname "$xsl_file")"

	variable=${xsl_file//\$\$ModuleName\$\$/$ModuleName}
	if [[ "$variable" =~ "\$\$" ]]; then
		variable="${variable#*\$\$}"
		variable="${variable%%\$\$*}"
		variable=.\$\$"$variable"\$\$
	else
		variable=""
	fi

	# magic: make xsl from Javascript
	cat "xsl-prefix${variable}.inc" > $xsl_file
	sed '
s/\&/\&amp;/g
s/</\&lt;/g
s/\/\/\&lt;xsl:sort/<xsl:sort/g
s/\/\/\&lt;xsl:when/<xsl:when/g
s/\/\/\&lt;xsl:otherwise/<xsl:otherwise/g
s/\$\$\(\w\+\)\$\$/\/*nbsp*\/<xsl:value-of select=\"ancestor-or-self::*\/@\1\"\/>/g
s/\/\/\&lt;xsl:/\/*nbsp*\/<xsl:/g
s/\/\/\&lt;\/xsl:/\/*nbsp*\/<\/xsl:/g
' < $js_file >> $xsl_file;
	cat "xsl-suffix${variable}.inc" >> $xsl_file
	#xmllint $xsl_file
done

rm -rf output.js
for xml_file in output.xml/*.xml; do
	ModuleName=${xml_file#output.xml/}
	ModuleName=${ModuleName%.xml}
	echo "Generate Javascript for $ModuleName"
	for xsl_file in $(find output.xsl -type f); do
		js_file=${xsl_file#output.xsl/}
		js_file=${js_file%.xsl}
		js_file=${js_file//\$\$ModuleName\$\$/$ModuleName}
		if [[ "$js_file" =~ "\$\$" ]]; then
			variable="${js_file#*\$\$}"
			variable="${variable%%\$\$*}"
			for var in $(xsltproc \$\$"${variable}"\$\$.xsl $xml_file); do
				js_file2=output.js/"${js_file//\$\$${variable}\$\$/$var}"
				xsltproc --stringparam $variable $var --output $js_file2 $xsl_file $xml_file 2>&1
				sed -i 's/\/\*nbsp\*\///g' $js_file2
			done
		else
			js_file=output.js/$js_file
			xsltproc --output $js_file $xsl_file $xml_file
			sed -i 's/\/\*nbsp\*\///g' $js_file
		fi
	done
done
