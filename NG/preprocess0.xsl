<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="xml" indent="yes" encoding="UTF-8" />

	<xsl:template match="@*|node()" priority="-1">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="text()[normalize-space()='']" />
	
	<xsl:template match="object/@type | object[@type]/field[value]" />
	<xsl:template match="object[@type]">
		<xsl:element name="{@type}">
			<xsl:for-each select="field[value]">
				<xsl:variable name="value" select="value" />
				<xsl:attribute name="{@name}">
					<xsl:choose>
						<xsl:when test="substring(value, 1, 1)='&quot;' and
							substring(value, string-length(value))='&quot;'">
							<xsl:value-of select="substring(value, 2, string-length(value) - 2)" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="value" />
						</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
			</xsl:for-each>
			<xsl:apply-templates select="@*|node()" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="object[@type]/field[not(value)]">
		<xsl:element name="{@name}">
			<xsl:apply-templates />
		</xsl:element>
	</xsl:template>
	
</xsl:stylesheet>
    		