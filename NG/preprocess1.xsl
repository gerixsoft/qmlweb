<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="xml" indent="yes" encoding="UTF-8" />

	<xsl:template match="@*|node()" priority="-1">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="Component/@name">
		<xsl:attribute name="ComponentName">
			<xsl:value-of select="." />
		</xsl:attribute>
	</xsl:template>

	<xsl:template match="Component/@prototype">
		<xsl:attribute name="PrototypeName">
			<xsl:value-of select="." />
		</xsl:attribute>
	</xsl:template>
	
	<xsl:template match="Enum/@name">
		<xsl:attribute name="EnumName">
			<xsl:value-of select="." />
		</xsl:attribute>
	</xsl:template>

	<xsl:template match="Signal/@name">
		<xsl:attribute name="SignalName">
			<xsl:value-of select="." />
		</xsl:attribute>
	</xsl:template>
	
	<xsl:template match="Parameter/@name">
		<xsl:attribute name="ParameterName">
			<xsl:value-of select="." />
		</xsl:attribute>
	</xsl:template>
	
	<xsl:template match="Method/@name">
		<xsl:attribute name="MethodName">
			<xsl:value-of select="." />
		</xsl:attribute>
	</xsl:template>
	
	<xsl:template match="Property/@name">
		<xsl:attribute name="PropertyName">
			<xsl:value-of select="." />
		</xsl:attribute>
	</xsl:template>
	
</xsl:stylesheet>
    		