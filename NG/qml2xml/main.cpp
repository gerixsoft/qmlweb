#include <iostream>
#include <QFile>

#include <QCoreApplication>
#include <qmljsdocument.h>
#include "qmljsreformatter.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    const QStringList args = app.arguments();
    if (args.length() == 1) {
        std::cerr << "usage: " << app.applicationName().toStdString() << " <list of qml files>" << std::endl;
        return 1;
    }

    for (int i = 1; i < args.size(); i++) {
        const QString& url = args[i];

        QFile qmlFile(url);
        qmlFile.open(QIODevice::ReadOnly);
        QString qml = qmlFile.readAll();
        qmlFile.close();

        QmlJS::Document::MutablePtr doc = QmlJS::Document::create(url, QmlJS::Language::Qml);
        doc->setSource(qml);
        doc->parse();
        QString xml = QmlJS::reformat(doc);

        QFile xmlFile(url + ".xml");
        xmlFile.open(QIODevice::WriteOnly);
        xmlFile.write("<qml>\n");
        xmlFile.write(xml.toUtf8());
        xmlFile.write("</qml>\n");
        xmlFile.close();
    }

    return 0;//app.exec();
}
