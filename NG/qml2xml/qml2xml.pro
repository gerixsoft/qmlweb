#-------------------------------------------------
#
# Project created by QtCreator 2014-10-27T14:09:33
#
#-------------------------------------------------

QT += core gui

DEFINES += QT_CREATOR
QMAKE_CXXFLAGS += -std=c++0x

TARGET = qml2xml
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    qmljsreformatter.cpp

HEADERS += \
    qmljsreformatter.h

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../Programs/Qt/Tools/QtCreator/lib/qtcreator/release/ -lQmlJS
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../Programs/Qt/Tools/QtCreator/lib/qtcreator/debug/ -lQmlJS
else:unix: LIBS += -L$$PWD/../../../../Programs/Qt/Tools/QtCreator/lib/qtcreator/ -lQmlJS

INCLUDEPATH += $$PWD/../../../qt-creator/src/libs/qmljs
DEPENDPATH += $$PWD/../../../qt-creator/src/libs/qmljs

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../Programs/Qt/Tools/QtCreator/lib/qtcreator/release/ -lLanguageUtils
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../Programs/Qt/Tools/QtCreator/lib/qtcreator/debug/ -lLanguageUtils
else:unix: LIBS += -L$$PWD/../../../../Programs/Qt/Tools/QtCreator/lib/qtcreator/ -lLanguageUtils

INCLUDEPATH += $$PWD/../../../qt-creator/src/libs
DEPENDPATH += $$PWD/../../../qt-creator/src/libs

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../Programs/Qt/Tools/QtCreator/lib/qtcreator/release/ -lCPlusPlus
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../Programs/Qt/Tools/QtCreator/lib/qtcreator/debug/ -lCPlusPlus
else:unix: LIBS += -L$$PWD/../../../../Programs/Qt/Tools/QtCreator/lib/qtcreator/ -lCPlusPlus

INCLUDEPATH += $$PWD/../../../qt-creator/src/libs/cplusplus
DEPENDPATH += $$PWD/../../../qt-creator/src/libs/cplusplus

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../Programs/Qt/Tools/QtCreator/lib/qtcreator/release/ -lUtils
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../Programs/Qt/Tools/QtCreator/lib/qtcreator/debug/ -lUtils
else:unix: LIBS += -L$$PWD/../../../../Programs/Qt/Tools/QtCreator/lib/qtcreator/ -lUtils

INCLUDEPATH += $$PWD/../../../qt-creator/src/libs
DEPENDPATH += $$PWD/../../../qt-creator/src/libs
