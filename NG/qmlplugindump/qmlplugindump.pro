#-------------------------------------------------
#
# Project created by QtCreator 2014-10-26T19:22:28
#
#-------------------------------------------------

QT       += core qml qml-private quick-private core-private

QT       -= gui

TARGET = qmlplugindump
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    qmlstreamwriter.cpp

HEADERS += \
    qmlstreamwriter.h
