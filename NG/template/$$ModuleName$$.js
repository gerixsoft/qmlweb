//<xsl:for-each select="Module/Component">
$$ComponentName$$ = function() {}
//<xsl:if test="@PrototypeName">
$$ComponentName$$.prototype = new $$PrototypeName$$();
//</xsl:if>//<xsl:for-each select="Enum">
// enum $$ComponentName$$::$$EnumName$$//<xsl:for-each select="values/object/field">
$$ComponentName$$.$$name$$ = //<xsl:value-of select="value"/>;//</xsl:for-each>
//</xsl:for-each>
// methods//<xsl:for-each select="Method">
$$ComponentName$$.prototype.$$MethodName$$ = function() {}//</xsl:for-each>

// properties//<xsl:for-each select="Property">
$$ComponentName$$.prototype.$$PropertyName$$ = Q_PROPERTY();//</xsl:for-each>

// signals//<xsl:for-each select="Signal">
$$ComponentName$$.prototype.$$SignalName$$ = Q_SIGNAL();//</xsl:for-each>
//</xsl:for-each>