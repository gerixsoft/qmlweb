<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output method="text" indent="no" encoding="UTF-8" />

	<xsl:param name="ComponentName" />

    <xsl:template match="/">
    	<xsl:for-each select="qml/Module/Component[@ComponentName=$ComponentName]">